# ====================================================
# Directory settings
# ====================================================
import os
import pickle
from typing import Callable, Any


ROOT_DIR = os.path.abspath(os.path.dirname(__file__))


def do_or_load(function: Callable, path_to_pickle: str) -> Any:
    if not os.path.isfile(path_to_pickle):
        with open(path_to_pickle, mode='wb') as f:
            result = function()
            pickle.dump(result, f)

    with open(path_to_pickle, 'rb') as file:
        result = pickle.load(file)
    return result


from typing import List, Callable, Any
import time
os.environ["TOKENIZERS_PARALLELISM"] = "false"

from pytorch_lightning.loggers import TensorBoardLogger

from un_processor.handle_text_to_sentences import Label, LabeledText, TextSentenceSeparator

OUTPUT_DIR = './output/mashaNLP/'
OUTPUT_DIR += str(time.time()) + '/'

ALL_LABELS = [
    'Affect-inclination-positive',
    'Affect-happiness-positive',
    'Affect-security-positive',
    'Affect-satisfaction-positive',
    'Judgement-normality-positive',
    'Judgement-capacity-positive',
    'Judgement-tenacity-positive',
    'Judgement-veracity-positive',
    'Judgement-propriety-positive',
    'Appreciation-impact-positive',
    'Appreciation-quality-positive',
    'Appreciation-balance-positive',
    'Appreciation-complexity-positive',
    'Appreciation-valuation-positive',
    'Affect-inclination-negative',
    'Affect-happiness--negative',
    'Affect-security-negative',
    'Affect-satisfaction-negative',
    'Judgement-normality-negative',
    'Judgement-capacity-negative',
    'Judgement-tenacity-negative',
    'Judgement-veracity-negative',
    'Judgement-propriety-negative',
    'Appreciation-impact-negative',
    'Appreciation-quality-negative',
    'Appreciation-balance-negative',
    'Appreciation-complexity-negative',
    'Appreciation-valuation-negative']


# ====================================================
# CFG
# ====================================================
class CFG:
    data_location = './un_data/admin.jsonl'
    wandb = False
    competition = 'UN'
    _wandb_kernel = 'nakama'
    debug = True
    apex = True
    print_freq = 100
    num_workers = 4
    model = "microsoft/deberta-base"
    scheduler = 'cosine'  # ['linear', 'cosine']
    batch_scheduler = True
    num_cycles = 0.5
    num_warmup_steps = 0
    epochs = 5
    encoder_lr = 2e-5
    decoder_lr = 2e-5
    min_lr = 1e-6
    eps = 1e-6
    betas = (0.9, 0.999)
    batch_size = 4
    fc_dropout = 0.2
    max_len = 512
    weight_decay = 0.01
    gradient_accumulation_steps = 3
    max_grad_norm = 1000
    seed = 42
    n_fold = 5
    trn_fold = [0, 1, 2, 3, 4]
    train = True
    log = True



# ====================================================
# Library
# ====================================================
import os
import gc
import ast
import time
import math
import random
import itertools
import warnings

warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
from tqdm.auto import tqdm
from sklearn.metrics import f1_score
from sklearn.model_selection import StratifiedKFold, GroupKFold, KFold

import torch
import torch.nn as nn
from torch.optim import Adam, SGD, AdamW
from torch.utils.data import DataLoader, Dataset

#
# pip install transformers==4.16.2
# pip install tokenizers==0.11.0
# %env TOKENIZERS_PARALLELISM=true


import tokenizers
import transformers

print(f"tokenizers.__version__: {tokenizers.__version__}")
print(f"transformers.__version__: {transformers.__version__}")
from transformers import AutoTokenizer, AutoModel, AutoConfig
from transformers import get_linear_schedule_with_warmup, get_cosine_schedule_with_warmup

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


## Helper functions
# From https://www.kaggle.com/theoviel/evaluation-metric-folds-baseline

def micro_f1(preds, truths):
    """
    Micro f1 on binary arrays.

    Args:
        preds (list of lists of ints): Predictions.
        truths (list of lists of ints): Ground truths.

    Returns:
        float: f1 score.
    """
    # Micro : aggregating over all instances
    preds = np.concatenate(preds)
    truths = np.concatenate(truths)
    return f1_score(truths, preds)


def spans_to_binary(spans, length=None):
    """
    Converts spans to a binary array indicating whether each character is in the span.

    Args:
        spans (list of lists of two ints): Spans.

    Returns:
        np array [length]: Binarized spans.
    """
    length = np.max(spans) if length is None else length
    binary = np.zeros(length)
    for start, end in spans:
        binary[start:end] = 1
    return binary


def span_micro_f1(preds, truths):
    """
    Micro f1 on spans.

    Args:
        preds (list of lists of two ints): Prediction spans.
        truths (list of lists of two ints): Ground truth spans.

    Returns:
        float: f1 score.
    """
    bin_preds = []
    bin_truths = []
    for pred, truth in zip(preds, truths):
        if not len(pred) and not len(truth):
            continue
        length = max(np.max(pred) if len(pred) else 0, np.max(truth) if len(truth) else 0)
        bin_preds.append(spans_to_binary(pred, length))
        bin_truths.append(spans_to_binary(truth, length))
    return micro_f1(bin_preds, bin_truths)


def create_labels_for_scoring(df):
    # example: ['0 1', '3 4'] -> ['0 1; 3 4']
    df['location_for_create_labels'] = [ast.literal_eval(f'[]')] * len(df)
    for i in range(len(df)):
        lst = df.loc[i, 'location']
        if lst:
            new_lst = ';'.join(lst)
            df.loc[i, 'location_for_create_labels'] = ast.literal_eval(f'[["{new_lst}"]]')
    # create labels
    truths = []
    for location_list in df['location_for_create_labels'].values:
        truth = []
        if len(location_list) > 0:
            location = location_list[0]
            for loc in [s.split() for s in location.split(';')]:
                start, end = int(loc[0]), int(loc[1])
                truth.append([start, end])
        truths.append(truth)
    return truths


def get_char_probs(texts, predictions, tokenizer):
    results = [np.zeros(len(t)) for t in texts]
    for i, (text, prediction) in enumerate(zip(texts, predictions)):
        encoded = tokenizer(text,
                            add_special_tokens=True,
                            return_offsets_mapping=True)
        for idx, (offset_mapping, pred) in enumerate(zip(encoded['offset_mapping'], prediction)):
            start = offset_mapping[0]
            end = offset_mapping[1]
            results[i][start:end] = pred
    return results


def get_results(char_probs, th=0.5):
    results = []
    for char_prob in char_probs:
        result = np.where(char_prob >= th)[0] + 1
        result = [list(g) for _, g in itertools.groupby(result, key=lambda n, c=itertools.count(): n - next(c))]
        result = [f"{min(r)} {max(r)}" for r in result]
        result = ";".join(result)
        results.append(result)
    return results


def get_predictions(results):
    predictions = []
    for result in results:
        prediction = []
        if result != "":
            for loc in [s.split() for s in result.split(';')]:
                start, end = int(loc[0]), int(loc[1])
                prediction.append([start, end])
        predictions.append(prediction)
    return predictions


print("""
# ====================================================
# Utils
# ====================================================
""")


def get_score(y_true, y_pred):
    score = span_micro_f1(y_true, y_pred)
    return score


def get_logger(filename=OUTPUT_DIR + 'train'):
    from logging import getLogger, INFO, StreamHandler, FileHandler, Formatter
    logger = getLogger(__name__)
    logger.setLevel(INFO)
    handler1 = StreamHandler()
    handler1.setFormatter(Formatter("%(message)s"))
    handler2 = FileHandler(filename=f"{filename}.log")
    handler2.setFormatter(Formatter("%(message)s"))
    logger.addHandler(handler1)
    logger.addHandler(handler2)
    return logger


def seed_everything(seed=42):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True


import json


def read_un_data(path: str):
    with open(path, mode='r') as f:
        data = f.read()

    for record in data.split('\n')[:-1]:
        yield json.loads(record, strict=False)


def json_to_labeled_text(json_dict: dict) -> LabeledText:
    labels = json_dict['label']
    text = json_dict['text']
    id_ = str(json_dict['id'])
    labels_data_class_objects = [Label(*label) for label in labels]

    return LabeledText(id_=id_, text=text, labels=labels_data_class_objects)


def sentence_to_correct_format(sentence: LabeledText, number_of_empty_labels: int = 2000):
    # 'id' would be label number + sentenceId
    # 'case_num' would be id of text
    # 'pn_num' would be sentenceId
    # 'pn_history' would be one sentence from wholo text
    # 'location' as ['1 10','15 17']
    # 'feature_text' is our label
    all_inputs = []
    excluded_labels = []
    if sentence.labels:
        if len(sentence.labels) > number_of_empty_labels:
            number_of_empty_labels = len(sentence.labels)

        for i, l in enumerate(sentence.labels):
            label_id = sentence.id_ + '_' + str(i)
            annotation = sentence.text[l.start_:l.end_]
            #                 ['id',     'case_num',                 'pn_num',         'feature_num', 'annotation?', 'location',               'feature_text', 'pn_history']
            all_inputs.append(
                [label_id, sentence.id_.split('_')[0], sentence.id_, None, [annotation], [f'{l.start_} {l.end_}'],
                 l.name, sentence.text])
            excluded_labels.append(l.name)

    negative_inputs = []
    for i, l in enumerate(ALL_LABELS):
        if l in excluded_labels:
            continue
        label_id = sentence.id_ + '_' + str(i)
        negative_inputs.append([label_id, sentence.id_.split('_')[0], sentence.id_, None, [], [], l, sentence.text])

    # use random subsample of them
    random.shuffle(negative_inputs)
    negative_inputs = negative_inputs[:number_of_empty_labels]

    all_inputs.extend(negative_inputs)
    previous_column_format = ['id', 'case_num', 'pn_num', 'feature_num', 'annotation', 'location', 'feature_text',
                              'pn_history']
    return pd.DataFrame(all_inputs, columns=previous_column_format)


from itertools import chain


def read_data(config=CFG, seed=42):
    seed_everything(seed=42)
    data_ = list(read_un_data(config.data_location))
    expected_columns_ = ['id', 'case_num', 'pn_num', 'feature_num', 'annotation', 'location', 'feature_text',
                         'pn_history']
    sentences_of_labeled_texts: List[LabeledText] = list(
        chain(*[TextSentenceSeparator.split(json_to_labeled_text(d), True) for d in data_ if len(d['label'])]))
    correct_type_objects = []
    for s in sentences_of_labeled_texts:
        correct_type_objects.append(sentence_to_correct_format(s))
    full_set = pd.concat(correct_type_objects)
    full_set.reset_index(drop=True, inplace=True)

    print("""
    # ====================================================
    # Data Loading
    # ====================================================
    """)
    train = full_set
    train['annotation_length'] = train['annotation'].apply(len)
    print(train['annotation_length'].value_counts())

    print("""
    # ====================================================
    # CV split
    # ====================================================""")
    Fold = GroupKFold(n_splits=config.n_fold)
    groups = train['pn_num'].values
    for n, (train_index, val_index) in enumerate(Fold.split(train, train['location'], groups)):
        train.loc[val_index, 'fold'] = int(n)
    train['fold'] = train['fold'].astype(int)
    print(train.groupby('fold').size())

    if config.debug:
        print(train.groupby('fold').size())
        train = train.sample(n=100, random_state=0).reset_index(drop=True)
        print(train.groupby('fold').size())

    print("""
    # ====================================================
    # tokenizer
    # ====================================================
    """)
    tokenizer = do_or_load(lambda: AutoTokenizer.from_pretrained(config.model), (config.model+'pickle').replace('/', ''))
    tokenizer.save_pretrained(OUTPUT_DIR + 'tokenizer/')
    config.tokenizer = tokenizer

    print("""
    # ====================================================
    # Define max_len
    # ====================================================
    """)
    for text_col in ['pn_history']:
        pn_history_lengths = []
        all_texts = train.groupby('pn_num').first(0)['pn_history'].values
        tk0 = tqdm(all_texts, total=len(all_texts))
        for text in tk0:
            length = len(tokenizer(text, add_special_tokens=False)['input_ids'])
            pn_history_lengths.append(length)
            # LOGGER.info(f'{text_col} max(lengths): {max(pn_history_lengths)}')

    for text_col in ['feature_text']:
        features_lengths = []

        tk0 = tqdm(ALL_LABELS, total=len(ALL_LABELS))
        for text in tk0:
            length = len(tokenizer(text, add_special_tokens=False)['input_ids'])
            features_lengths.append(length)
        # LOGGER.info(f'{text_col} max(lengths): {max(features_lengths)}')

    config.max_len = max(pn_history_lengths) + max(features_lengths) + 3  # cls & sep & sep
    # LOGGER.info(f"max_len: {config.max_len}")
    return train


print("""
# ====================================================
# Dataset
# ====================================================
""")


def prepare_input(cfg, text, feature_text):
    inputs = cfg.tokenizer(text, feature_text,
                           add_special_tokens=True,
                           max_length=cfg.max_len,
                           padding="max_length",
                           return_offsets_mapping=False)
    for k, v in inputs.items():
        inputs[k] = torch.tensor(v, dtype=torch.long)
    return inputs


def create_label(cfg, text, annotation_length, location_list):
    encoded = cfg.tokenizer(text,
                            add_special_tokens=True,
                            max_length=cfg.max_len,
                            padding="max_length",
                            return_offsets_mapping=True)
    offset_mapping = encoded['offset_mapping']
    ignore_idxes = np.where(np.array(encoded.sequence_ids()) != 0)[0]
    label = np.zeros(len(offset_mapping))
    label[ignore_idxes] = -1
    if annotation_length != 0:
        for location in location_list:
            for loc in [s.split() for s in location.split(';')]:
                start_idx = -1
                end_idx = -1
                start, end = int(loc[0]), int(loc[1])
                for idx in range(len(offset_mapping)):
                    if (start_idx == -1) & (start < offset_mapping[idx][0]):
                        start_idx = idx - 1
                    if (end_idx == -1) & (end <= offset_mapping[idx][1]):
                        end_idx = idx + 1
                if start_idx == -1:
                    start_idx = end_idx
                if (start_idx != -1) & (end_idx != -1):
                    label[start_idx:end_idx] = 1
    return torch.tensor(label, dtype=torch.float)


class TrainDataset(Dataset):
    def __init__(self, cfg, df):
        self.cfg = cfg
        self.feature_texts = df['feature_text'].values
        self.pn_historys = df['pn_history'].values
        self.annotation_lengths = df['annotation_length'].values
        self.locations = df['location'].values

    def __len__(self):
        return len(self.feature_texts)

    def __getitem__(self, item):
        inputs = prepare_input(self.cfg,
                               self.pn_historys[item],
                               self.feature_texts[item])
        label = create_label(self.cfg,
                             self.pn_historys[item],
                             self.annotation_lengths[item],
                             self.locations[item])
        return inputs, label


print("""
# ====================================================
# Model
# ====================================================
""")


class CustomModel(nn.Module):
    def __init__(self, cfg, config_path=None, pretrained=False):
        super().__init__()
        self.cfg = cfg
        if config_path is None:
            self.config = AutoConfig.from_pretrained(cfg.model, output_hidden_states=True)
        else:
            self.config = torch.load(config_path)
        if pretrained:
            self.model = AutoModel.from_pretrained(cfg.model, config=self.config)
        else:
            self.model = AutoModel(self.config)
        self.fc_dropout = nn.Dropout(cfg.fc_dropout)
        self.fc = nn.Linear(self.config.hidden_size, 1)
        self._init_weights(self.fc)

    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            module.weight.data.normal_(mean=0.0, std=self.config.initializer_range)
            if module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.Embedding):
            module.weight.data.normal_(mean=0.0, std=self.config.initializer_range)
            if module.padding_idx is not None:
                module.weight.data[module.padding_idx].zero_()
        elif isinstance(module, nn.LayerNorm):
            module.bias.data.zero_()
            module.weight.data.fill_(1.0)

    def feature(self, inputs):
        outputs = self.model(**inputs)
        last_hidden_states = outputs[0]
        return last_hidden_states

    def forward(self, inputs):
        feature = self.feature(inputs)
        output = self.fc(self.fc_dropout(feature))
        return output


print("""
# ====================================================
# Helper functions
# ====================================================
""")


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def asMinutes(s):
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


def timeSince(since, percent):
    now = time.time()
    s = now - since
    es = s / (percent)
    rs = es - s
    return '%s (remain %s)' % (asMinutes(s), asMinutes(rs))


class Trainer:
    def __init__(self, config, train_data):
        self.config = config
        self.train_data = train_data
        self.tb_logger = TensorBoardLogger(save_dir=OUTPUT_DIR)
        self.global_step = 0
        self.global_epoch = 0

    def train_fn(self, fold, train_loader, model, criterion, optimizer, epoch, scheduler, device):
        config = self.config
        model.train()
        scaler = torch.cuda.amp.GradScaler(enabled=config.apex)
        losses = AverageMeter()
        start = end = time.time()
        for step, (inputs, labels) in enumerate(train_loader):
            for k, v in inputs.items():
                inputs[k] = v.to(device)
            labels = labels.to(device)
            batch_size = labels.size(0)
            with torch.cuda.amp.autocast(enabled=config.apex):
                y_preds = model(inputs)
            loss = criterion(y_preds.view(-1, 1), labels.view(-1, 1))
            loss = torch.masked_select(loss, labels.view(-1, 1) != -1).mean()
            if config.gradient_accumulation_steps > 1:
                loss = loss / config.gradient_accumulation_steps
            losses.update(loss.item(), batch_size)
            scaler.scale(loss).backward()
            grad_norm = torch.nn.utils.clip_grad_norm_(model.parameters(), config.max_grad_norm)
            if (step + 1) % config.gradient_accumulation_steps == 0:
                scaler.step(optimizer)
                scaler.update()
                optimizer.zero_grad()
                self.global_step += 1
                if config.batch_scheduler:
                    scheduler.step()
            end = time.time()

            if self.global_step % config.print_freq == 0 or self.global_step == (len(train_loader) - 1):
                self.tb_logger.log_metrics(
                    dict(
                    train_loss = losses.avg,
                    grad_norm = grad_norm,
                    lr = scheduler.get_lr()[0]
                    ), step=self.global_step)
                print('Epoch: [{0}][{1}/{2}] '
                      'Elapsed {remain:s} '
                      'Loss: {loss.val:.4f}({loss.avg:.4f}) '
                      'Grad: {grad_norm:.4f}  '
                      'LR: {lr:.8f}  '
                      .format(epoch + 1, step, len(train_loader),
                              remain=timeSince(start, float(step + 1) / len(train_loader)),
                              loss=losses,
                              grad_norm=grad_norm,
                              lr=scheduler.get_lr()[0]))
                avg_val_loss, predictions = self.valid_fn(self.valid_loader, model, criterion, device)
                self.best_score = self._compare_val_loss(predictions, self.best_score, model, fold)


        return losses.avg

    def valid_fn(self, valid_loader, model, criterion, device, log=True):
        losses = AverageMeter()
        model.eval()
        preds = []
        start = end = time.time()
        for step, (inputs, labels) in enumerate(valid_loader):
            self.elapsed = time.time() - self.start_time
            for k, v in inputs.items():
                inputs[k] = v.to(device)
            labels = labels.to(device)
            batch_size = labels.size(0)
            with torch.no_grad():
                y_preds = model(inputs)
            loss = criterion(y_preds.view(-1, 1), labels.view(-1, 1))
            loss = torch.masked_select(loss, labels.view(-1, 1) != -1).mean()
            if self.config.gradient_accumulation_steps > 1:
                loss = loss / self.config.gradient_accumulation_steps
            losses.update(loss.item(), batch_size)
            preds.append(y_preds.sigmoid().to('cpu').numpy())
            end = time.time()
            if step % self.config.print_freq == 0 or step == (len(valid_loader) - 1):
                print('EVAL: [{0}/{1}] '
                      'Elapsed {remain:s} '
                      'Loss: {loss.val:.4f}({loss.avg:.4f}) '
                      .format(step, len(valid_loader),
                              loss=losses,
                              remain=timeSince(start, float(step + 1) / len(valid_loader))))
        predictions = np.concatenate(preds)

        LOGGER.info(
            f'Epoch {self.global_epoch + 1} - avg_val_loss: {losses.avg:.4f}  time: {self.elapsed:.0f}s')
        LOGGER.info(f'Epoch {self.global_epoch + 1} - Score: {self.global_epoch:.4f}')

        if log:
            self.tb_logger.log_metrics(dict(val_loss = losses.avg), step=self.global_step)

        return losses.avg, predictions

    def inference_fn(self, test_loader, model, device):
        preds = []
        model.eval()
        model.to(device)
        tk0 = tqdm(test_loader, total=len(test_loader))
        for inputs in tk0:
            for k, v in inputs.items():
                inputs[k] = v.to(device)
            with torch.no_grad():
                y_preds = model(inputs)
            preds.append(y_preds.sigmoid().to('cpu').numpy())
        predictions = np.concatenate(preds)
        return predictions

    def _compare_val_loss(self, predictions, best_score, model, fold):
        predictions = predictions.reshape((len(self.valid_folds), self.config.max_len))

        # scoring
        char_probs = get_char_probs(self.valid_texts, predictions, self.config.tokenizer)
        results = get_results(char_probs, th=0.5)
        preds = get_predictions(results)
        score = get_score(self.valid_labels, preds)

        self.elapsed = time.time() - self.start_time

        if best_score < score:
            best_score = score
            LOGGER.info(f'STEP {self.global_step} - Save Best Score: {best_score:.4f} Model')
            torch.save({'model': model.state_dict(),
                        'predictions': predictions},
                       OUTPUT_DIR + f"{self.config.model.replace('/', '-')}_fold{fold}_best.pth")
        self.tb_logger.log_metrics(dict(best_score=best_score), step=self.global_step)
        return best_score

    print("""
    # ====================================================
    # train loop
    # ====================================================
    """)

    def train_loop(self, folds, fold):
        LOGGER.info(f"========== fold: {fold} training ==========")

        # ====================================================
        # loader
        # ====================================================
        train_folds = folds[folds['fold'] != fold].reset_index(drop=True)
        valid_folds = folds[folds['fold'] == fold].reset_index(drop=True)
        self.valid_folds = valid_folds
        valid_texts = valid_folds['pn_history'].values
        self.valid_texts = valid_texts
        valid_labels = create_labels_for_scoring(valid_folds)
        self.valid_labels = valid_labels

        train_dataset = TrainDataset(self.config, train_folds)
        valid_dataset = TrainDataset(self.config, valid_folds)

        train_loader = DataLoader(train_dataset,
                                  batch_size=self.config.batch_size,
                                  shuffle=True,
                                  num_workers=self.config.num_workers, pin_memory=True, drop_last=True)
        # Warning! can cause GPU memory leak
        self.train_loader = train_loader
        valid_loader = DataLoader(valid_dataset,
                                  batch_size=self.config.batch_size,
                                  shuffle=False,
                                  num_workers=self.config.num_workers, pin_memory=True, drop_last=False)
        # Warning! can cause GPU memory leak
        self.valid_loader = valid_loader

        # ====================================================
        # model & optimizer
        # ====================================================
        model = CustomModel(self.config, config_path=None, pretrained=True)
        torch.save(model.config, OUTPUT_DIR + 'config.pth')
        model.to(device)

        def get_optimizer_params(model, encoder_lr, decoder_lr, weight_decay=0.0):
            param_optimizer = list(model.named_parameters())
            no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]
            optimizer_parameters = [
                {'params': [p for n, p in model.model.named_parameters() if not any(nd in n for nd in no_decay)],
                 'lr': encoder_lr, 'weight_decay': weight_decay},
                {'params': [p for n, p in model.model.named_parameters() if any(nd in n for nd in no_decay)],
                 'lr': encoder_lr, 'weight_decay': 0.0},
                {'params': [p for n, p in model.named_parameters() if "model" not in n],
                 'lr': decoder_lr, 'weight_decay': 0.0}
            ]
            return optimizer_parameters

        optimizer_parameters = get_optimizer_params(model,
                                                    encoder_lr=self.config.encoder_lr,
                                                    decoder_lr=self.config.decoder_lr,
                                                    weight_decay=self.config.weight_decay)
        optimizer = AdamW(optimizer_parameters, lr=self.config.encoder_lr, eps=self.config.eps, betas=self.config.betas)

        # ====================================================
        # scheduler
        # ====================================================
        def get_scheduler(cfg, optimizer, num_train_steps):
            if cfg.scheduler == 'linear':
                scheduler = get_linear_schedule_with_warmup(
                    optimizer, num_warmup_steps=cfg.num_warmup_steps, num_training_steps=num_train_steps
                )
            elif cfg.scheduler == 'cosine':
                scheduler = get_cosine_schedule_with_warmup(
                    optimizer, num_warmup_steps=cfg.num_warmup_steps, num_training_steps=num_train_steps,
                    num_cycles=cfg.num_cycles
                )
            return scheduler

        num_train_steps = int(len(train_folds) / self.config.batch_size * self.config.epochs)
        scheduler = get_scheduler(self.config, optimizer, num_train_steps)

        # ====================================================
        # loop
        # ====================================================
        criterion = nn.BCEWithLogitsLoss(reduction="none")

        self.best_score = -1.

        for epoch in range(self.config.epochs):
            self.start_time = time.time()

            # train
            avg_loss = self.train_fn(fold, train_loader, model, criterion, optimizer, epoch, scheduler, device)

            # eval
            avg_val_loss, predictions = self.valid_fn(valid_loader, model, criterion, device, log=False)
            self.best_score = self._compare_val_loss(predictions, self.best_score, model, fold)
            self.global_epoch += 1

        predictions = torch.load(OUTPUT_DIR + f"{self.config.model.replace('/', '-')}_fold{fold}_best.pth",
                                 map_location=torch.device('cpu'))['predictions']
        valid_folds[[i for i in range(self.config.max_len)]] = predictions

        torch.cuda.empty_cache()
        gc.collect()

        return valid_folds

    def get_result(self, oof_df):
        labels = create_labels_for_scoring(oof_df)
        predictions = oof_df[[i for i in range(self.config.max_len)]].values
        char_probs = get_char_probs(oof_df['pn_history'].values, predictions, self.config.tokenizer)
        results = get_results(char_probs, th=0.5)
        preds = get_predictions(results)
        score = get_score(labels, preds)
        LOGGER.info(f'Score: {score:<.4f}')

    def run_training(self):
        oof_df = pd.DataFrame()
        for fold in range(self.config.n_fold):
            if fold in self.config.trn_fold:
                _oof_df = self.train_loop(self.train_data, fold)
                oof_df = pd.concat([oof_df, _oof_df])
                LOGGER.info(f"========== fold: {fold} result ==========")
                self.get_result(_oof_df)
        oof_df = oof_df.reset_index(drop=True)
        LOGGER.info(f"========== CV ==========")
        self.get_result(oof_df)
        oof_df.to_pickle(OUTPUT_DIR + 'oof_df.pkl')


if __name__ == '__main__':
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    LOGGER = get_logger()

    if CFG.debug:
        CFG.epochs = 3
        CFG.trn_fold = [0]
        CFG.print_freq = 9

    data = read_data(CFG)
    t = Trainer(CFG, data)
    t.run_training()
