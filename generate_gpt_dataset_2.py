import json
import os

import pandas as pd
import numpy as np

import run_training_masha as rm

rm.OUTPUT_DIR = './output/mashaNLP/'
DATA_FILE_NAME = 'full_text.csv'
DATA_FILE_PATH = os.path.join(rm.OUTPUT_DIR, DATA_FILE_NAME)
rm.CFG.simple = False
rm.CFG.debug = False
CSV_SEPARATOR = '#'

from transformers import GPT2Tokenizer

tokenizer = rm.do_or_load((lambda: GPT2Tokenizer.from_pretrained("gpt2")), 'gpt2_tok')

PRICES_PER_TOKEN = tuple(p/ 1000 for p in [0.0004, 0.0006, 0.003, 0.03])
EPOCHS = 4
dir_name = 'GPT_DATA_2'

def estimate_costs(tokens: int, epochs=EPOCHS, ppts = PRICES_PER_TOKEN):
    return '\n'.join([str(tokens * p * epochs) +' USD Tokens: ' + str(tokens) for p in ppts])


inputs = tokenizer("Hello, my dog is cute", return_tensors="pt")

if __name__ == '__main__':
    if not os.path.exists(rm.OUTPUT_DIR):
        os.makedirs(rm.OUTPUT_DIR)

    if True or not os.path.isfile(DATA_FILE_PATH):
        data = rm.read_data(rm.CFG)
        data['feature_text'] = data['feature_text'].apply(lambda s: s.replace('--', '-'))
        assert all([CSV_SEPARATOR not in v for v in data['pn_history'].values]), 'Use other separator'
        data.to_csv(DATA_FILE_PATH, index=False, sep=CSV_SEPARATOR)
    data = pd.read_csv(DATA_FILE_PATH, sep=CSV_SEPARATOR)
    data.reset_index(inplace=True)

    rows = []
    json_rows = []
    for key, group in data.groupby('pn_num'):
        r = group.iloc[0]
        row_as_dict = dict(id_=r['id'], fold=str(r['fold']), prompt=r['pn_history'] + ' -->> ')
        completion = ' '

        contains_labels = bool(len(group['location'] !='[]'))
        completion+=str(contains_labels)

        completion += ' Others: '

        completion_labels = ''

        for i, label in enumerate(sorted(rm.ALL_LABELS)):
            label_annotations = group[group.feature_text == label]
            contains_label = label_annotations['annotation_length'].values.sum() != 0
            #
            completion += ' ' + str(i) + '. ' + str(contains_label)
            #

            if not contains_label:
                continue
            label = label.replace('--', '-')
            completion_labels += f'<label{str(i)}. > '
            if contains_label:
                for j, (_, r) in enumerate(label_annotations.iterrows()):
                    completion_labels += f'< {str(j)}.>'
                    completion_labels += r['annotation'][2:-2]
                    completion_labels += f'</{str(j)}.> '
            completion_labels += f'</label {str(i)}.> '

        completion+=' @@@ '
        completion+=completion_labels

        completion += ' ###'
        row_as_dict['completion'] = completion
        rows.append(row_as_dict)


        row_as_dict['len_prompt'] = len(tokenizer(row_as_dict['prompt'], return_tensors="pt")['input_ids'][0])
        row_as_dict['len_completion'] = len(tokenizer(row_as_dict['completion'], return_tensors="pt")['input_ids'][0])

        json_data = json.dumps(row_as_dict, ensure_ascii=False)
        json_rows.append(json_data)

        row_as_dict['tok_prompt'] = tokenizer(row_as_dict['prompt'], return_tensors="pt")['input_ids'][0]
        row_as_dict['tok_completion'] = tokenizer(row_as_dict['completion'], return_tensors="pt")['input_ids'][0]

    for fold in np.unique(data['fold'].values):
        selected_lines_train = [jr for r, jr in zip(rows, json_rows) if int(r['fold']) != fold]
        selected_lines_test = [jr for r, jr in zip(rows, json_rows) if int(r['fold']) == fold]

        costs_train = estimate_costs(
            sum([r['len_prompt'] + r['len_completion'] for r, jr in zip(rows, json_rows) if int(r['fold']) != fold]))
        costs_val = estimate_costs(
            sum([r['len_prompt'] + r['len_completion'] for r, jr in zip(rows, json_rows) if int(r['fold']) == fold]))

        # save a file
        result_path = os.path.join(rm.OUTPUT_DIR, dir_name, f'fold_{str(fold)}_train.jsonl')
        os.makedirs(os.path.dirname(result_path), exist_ok=True)
        with open(result_path, mode='w') as f:
            f.write('\n'.join(selected_lines_train))

        # save a file
        result_path = os.path.join(rm.OUTPUT_DIR, dir_name, f'fold_{str(fold)}_val.jsonl')
        os.makedirs(os.path.dirname(result_path), exist_ok=True)
        with open(result_path, mode='w') as f:
            f.write('\n'.join(selected_lines_test))

        pricing_path = os.path.join(rm.OUTPUT_DIR, dir_name, f'fold_{str(fold)}_prices.txt')
        with open(pricing_path, mode='w') as f:
            f.write('\nTRAIN_SET\n')
            f.write(costs_train)
            f.write('\nVAL_SET\n')
            f.write(costs_val)

    a = 0
