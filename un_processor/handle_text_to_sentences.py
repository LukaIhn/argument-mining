from copy import deepcopy
from dataclasses import dataclass
from typing import List, Tuple, NamedTuple

from itertools import chain
import nltk

# need to download its model for nltk to work
try:
    nltk.sent_tokenize('')
except:
    nltk.download('punkt')


@dataclass
class Label:
    start_: int
    end_: int
    name: str


@dataclass
class LabeledText:
    text: str
    labels: List[Label] = None
    suffix: str = ''
    id_: str = ''

    def __len__(self):
        return len(self.text) + len(self.suffix)

    def append_label(self, label: Label):
        if self.labels is None:
            self.labels = list()
        self.labels.append(label)


class TextSentenceSeparator:
    @classmethod
    def split(cls, text: LabeledText, update_id: bool = False) -> List[LabeledText]:
        sentences_without_labels = cls._split_into_sentences(text.text)

        if update_id:
            cls._update_ids(sentences_without_labels, text.id_)

        if text.labels:
            return cls._add_labels(sentences_without_labels, text.labels)

        return sentences_without_labels

    @classmethod
    def _update_ids(cls, sentences: List[LabeledText], id_: str) -> None:
        """Updates inplace"""
        for i, sentence in enumerate(sentences):
            sentence.id_ = f'{id_}_{str(i)}'

    @classmethod
    def _split_into_sentences(cls, text: str) -> List[LabeledText]:
        sentences = nltk.sent_tokenize(text)
        separated_pairs = []
        remaining_text = text
        for sentence_number, sentence in enumerate(sentences):
            is_last = sentence_number == len(sentences) - 1
            if is_last:
                suffix = remaining_text[len(sentence):]
            else:
                # cut out sentence
                remaining_text = remaining_text[len(sentence):]

                sentence_start = remaining_text.find(sentences[sentence_number + 1])
                suffix = remaining_text[:sentence_start]
                assert len(suffix), 'All but last should have suffix!'

                # cut out suffix
                remaining_text = remaining_text[len(suffix):]

            separated_pairs.append(LabeledText(sentence, suffix=suffix))

        return separated_pairs

    @classmethod
    def _add_labels(cls, sentences_after_split: List[LabeledText], labels_before_split: List[Label]) \
            -> List[LabeledText]:
        sentences_after_split = deepcopy(sentences_after_split)
        for label in labels_before_split:
            for i, sentence in enumerate(sentences_after_split):
                assert label.start_ >= 0
                assert label.end_ >= 0
                assert label.start_ < label.end_

                label_starts_inside = label.start_ < len(sentence)
                label_ends_inside = label.end_ <= len(sentence)

                if not label_starts_inside:
                    # skip sentence
                    skipped_chars = len(sentence)
                    assert i != len(sentences_after_split), 'This should not happen! Bug in implementation!'
                    # shift label indices
                    label = Label(label.start_ - skipped_chars, label.end_ - skipped_chars, label.name)
                    continue
                elif not label_ends_inside:
                    # limit length of label to be same as current sentence
                    splitted_label = Label(label.start_, len(sentence.text), label.name)

                    sentence.append_label(splitted_label)

                    # update label indices
                    label = Label(0, label.end_ - len(sentence), label.name)
                    continue
                else:
                    # the end of for cycle
                    label_end = min(label.end_, len(sentence.text))
                    sentence.append_label(Label(label.start_, label_end, label.name))
                    break

        return sentences_after_split

    @staticmethod
    def join(sentences):
        pass


test_cases = [
    (LabeledText('Hello! How are you?'),
     [LabeledText('Hello!', suffix=' '), LabeledText('How are you?', suffix='')]),
    (LabeledText('\n\nHello!\n\nHow are you?\n\n'),
     [LabeledText('\n\nHello!', suffix='\n\n'), LabeledText('How are you?', suffix='\n\n')]),
    # Negative cases
    (LabeledText('Hello!How are you?'),
     [LabeledText('Hello!How are you?')]),
    (LabeledText('Hello, Mr.President!'),
     [LabeledText('Hello, Mr.President!')]),
    (LabeledText('Hello, Mr. President!'),
     [LabeledText('Hello, Mr. President!')]),
    (LabeledText('Hello, Dr. Smith!'),
     [LabeledText('Hello, Dr. Smith!')]),

    # Attention for this!
    (LabeledText('Hello, MUDr. Smith!'),
     [LabeledText('Hello, MUDr.', suffix=' '), LabeledText('Smith!')]),
]

TEST_SUFFIX = ' '
TEST_SENTENCE = 'Hello!'
TEST_STRING = TEST_SENTENCE + TEST_SUFFIX
test_cases_with_labels = [
    # label on first sentence
    (LabeledText(TEST_STRING + TEST_STRING, [Label(0, len(TEST_SENTENCE), '')]),
     [LabeledText(TEST_SENTENCE, [Label(0, len(TEST_SENTENCE), '')], suffix=TEST_SUFFIX),
      LabeledText(TEST_SENTENCE, suffix=TEST_SUFFIX), ]),
    # label on second sentence
    (LabeledText(TEST_STRING + TEST_STRING, [Label(len(TEST_STRING), 2 * len(TEST_STRING), '')]),
     [LabeledText(TEST_SENTENCE, suffix=TEST_SUFFIX),
      LabeledText(TEST_SENTENCE, [Label(0, len(TEST_SENTENCE), '')], suffix=TEST_SUFFIX), ]),
    # label on both sentences for "ello" "ello"
    (LabeledText(TEST_STRING + TEST_STRING, [Label(1, len(TEST_SENTENCE) - 1, ''),
                                             Label(len(TEST_STRING) + 1, len(TEST_STRING) + len(TEST_SENTENCE) - 1,
                                                      '')]),
     [LabeledText(TEST_SENTENCE, [Label(1, len(TEST_SENTENCE) - 1, '')], suffix=TEST_SUFFIX),
      LabeledText(TEST_SENTENCE, [Label(1, len(TEST_SENTENCE) - 1, '')], suffix=TEST_SUFFIX), ]),
    # label overlaps over both
    (LabeledText(TEST_STRING + TEST_STRING, [Label(0, 2 * len(TEST_STRING), '')]),
     [LabeledText(TEST_SENTENCE, [Label(0, len(TEST_SENTENCE), '')], suffix=TEST_SUFFIX),
      LabeledText(TEST_SENTENCE, [Label(0, len(TEST_SENTENCE), '')], suffix=TEST_SUFFIX), ]),
]

test_text = """
While I am in the United States, I should like first of all, on behalf of the leadership and the people of Russia, to express profound condolences to the Government and the people of the United States of America following the tragic death of the crew of the space shuttle Columbia. We share the grief of our American partners, with whom we are actively cooperating in outer space, primarily in the implementation of the international space station project.
"""


def test_length_of_text_same_as_sentences():
    splitted_text = TextSentenceSeparator.split(LabeledText(text=test_text))
    assert len(test_text) == sum([len(s) for s in splitted_text])

def test_to_sentences():
    for input_, expected_new_pairs in test_cases:
        actual_pairs = TextSentenceSeparator.split(input_)
        assert len(expected_new_pairs) == len(actual_pairs)
        # assert len(input_) == sum([len(s) for s in actual_pairs])

        for a, b in zip(actual_pairs, expected_new_pairs):
            assert a == b


def test_to_sentences_with_labels():
    for input_, expected_new_pairs in test_cases_with_labels:
        actual_pairs = TextSentenceSeparator.split(input_)
        assert len(expected_new_pairs) == len(actual_pairs)
        for a, b in zip(actual_pairs, expected_new_pairs):
            assert a == b
            TextSentenceSeparator.split(input_)

if __name__ == '__main__':
    # test_length_of_text_same_as_sentences()
    test_to_sentences()
    test_to_sentences_with_labels()
