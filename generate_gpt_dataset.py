import json
import os

import pandas as pd
import numpy as np

import run_training_masha as rm

rm.OUTPUT_DIR = './output/mashaNLP/'
DATA_FILE_NAME = 'full_text.csv'
DATA_FILE_PATH = os.path.join(rm.OUTPUT_DIR, DATA_FILE_NAME)
rm.CFG.simple = False
rm.CFG.debug = False
CSV_SEPARATOR = '#'

from transformers import GPT2Tokenizer, GPT2Model
import torch
tokenizer = GPT2Tokenizer.from_pretrained("gpt2")


PRICE_PER_TOKEN = 0.0004/1000
EPOCHS = 4
def estimate_costs(tokens: int, epochs=EPOCHS):
    print(tokens*PRICE_PER_TOKEN*epochs, ' USD')

# inputs = tokenizer("Hello, my dog is cute", return_tensors="pt")



if __name__ == '__main__':
    if not os.path.exists(rm.OUTPUT_DIR):
        os.makedirs(rm.OUTPUT_DIR)

    if True or not os.path.isfile(DATA_FILE_PATH):
        data = rm.read_data(rm.CFG)
        data['feature_text'] = data['feature_text'].apply(lambda s: s.replace('--', '-'))
        assert all([CSV_SEPARATOR not in v for v in data['pn_history'].values]), 'Use other separator'
        data.to_csv(DATA_FILE_PATH, index=False, sep=CSV_SEPARATOR)
    data = pd.read_csv(DATA_FILE_PATH, sep=CSV_SEPARATOR)
    data.reset_index(inplace=True)

    rows = []
    json_rows = []
    for i, r in data.iterrows():
        row_as_dict = dict(id_=r['id'], fold=r['fold'], prompt=r['pn_history'] + ' -->> ' + r['feature_text'] + '(',
                           completion=' ' + str(r['annotation_length'] != 0) + ' )' + r['annotation'] + ' ###')
        row_as_dict['len_prompt'] = len(tokenizer(row_as_dict['prompt'], return_tensors="pt")['input_ids'][0])
        row_as_dict['len_completion'] = len(tokenizer(row_as_dict['completion'], return_tensors="pt")['input_ids'][0])

        rows.append(row_as_dict)

        json_data = json.dumps(row_as_dict, ensure_ascii=False)
        json_rows.append(json_data)

    for fold in np.unique(data['fold'].values):
        selected_lines_train = [jr for r, jr in zip(rows, json_rows) if r['fold'] != fold]
        selected_lines_test = [jr for r, jr in zip(rows, json_rows) if r['fold'] == fold]

        estimate_costs(sum([r['len_prompt']+r['len_completion'] for r, jr in zip(rows, json_rows) if r['fold'] != fold]))
        estimate_costs(sum([r['len_prompt']+r['len_completion'] for r, jr in zip(rows, json_rows) if r['fold'] == fold]))

        # save a file
        result_path = os.path.join(rm.OUTPUT_DIR, 'GPT_DATA', f'fold_{str(fold)}_train.jsonl')
        os.makedirs(os.path.dirname(result_path), exist_ok=True)
        with open(result_path, mode='w') as f:
            f.write('\n'.join(selected_lines_train))

        # save a file
        result_path = os.path.join(rm.OUTPUT_DIR, 'GPT_DATA', f'fold_{str(fold)}_val.jsonl')
        os.makedirs(os.path.dirname(result_path), exist_ok=True)
        with open(result_path, mode='w') as f:
            f.write('\n'.join(selected_lines_test))

    a = 0
